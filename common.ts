import { Lex, Language, Rules, NStart } from "./definitions"

export {
  first
}

// TODO Canonical - with empty check
function first<T extends Lex, N extends Lex, L extends Language<T,N>>(lang: L, rules: Rules<L>) {
  const {terminals, notTerminals} = lang
  , {length} = notTerminals
  , $return: Map<N|NStart, any[]> = new Map()

  for (let i = length; i--;)
    $return.set(notTerminals[i], [])
  for (let i = rules.length; i--;) {
    const {0: notTerminal, 1: letter} = rules[i]
    if (notTerminal === letter)
      continue

    const firsts = $return.get(notTerminal)!
    if (terminals.includes(letter as T))
      firsts.push(letter)
    /* istanbul ignore next */
    else if (notTerminals.includes(letter as N))
      firsts.push($return.get(letter as N))
    /* istanbul ignore next */
    else
      throw new Error(`Unknown first letter "${letter}" in rule #${i}`)
  }
  for (let i = length; i--;) {
    const notTerminal = notTerminals[i]
    , firsts = $return.get(notTerminal)!
    $return.set(notTerminal, [...new Set(...firsts.flat(Infinity))])
  }
  return $return as Map<N, T[]>
}
