import {
  Start,
  end
} from "./consts"

//TODO with metrics
export type Lex = string

export type TEnd = typeof end
export type NStart = typeof Start

//TODO Map?
export type Language<T extends Lex = Lex, N extends Lex = Lex> = {
  //TODO Set & indexing
  terminals: readonly T[]
  notTerminals: readonly N[]
}

export type Terminal<L extends Language> = L["terminals"][number]
export type NotTerminal<L extends Language> = L["notTerminals"][number]
export type Letter<L extends Language> = (L["notTerminals"]|L["terminals"])[number]

//TODO extending function
//TODO Object|Map
export type Rules<L extends Language> = [
  [NStart, NotTerminal<L>],
  //TODO 2d Array
  ...Array<[
    NotTerminal<L>,
    Letter<L>,
    ...Letter<L>[]
  ]>
]


export type LR1Table<L extends Language> = Array<{
  /** Actions subtable */
  a: Partial<{
    /** action */
    [prop in TEnd|Terminal<L>]:
      /** accept */
      true
      | [
        /** `false` is shift, `true` is reduce. //TODO replace with 0|1 */
        boolean,
        // 
        number
      ]
  }>
  /** GoTo subtable */
  g?: Partial<{
    /** goto */
    [prop in NStart|NotTerminal<L>]: number
  }>
}>

export type Item<L extends Language = Language> = Map<string, Situation<L>>

//TODO strict keys
export type Situation<L extends Language = Language> = readonly [
  /** Rule */
  number,
  /** Pointer */
  number,
  Terminal<L>|TEnd
]