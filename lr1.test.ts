import { parser, closure, gotoFn, itemFromSituations } from './lr1'
import { errors } from './params.json'
import { Item, LR1Table } from './definitions'
import { end } from "./consts"
import { lang1, rules1, ast1, str1, arr1 } from './lr1.test-data'

describe('1', () => {
  const states1: LR1Table<typeof lang1> = Object.values({
    0: {
      a: {
        "i": [false, 6]
      },
      g: {
        "E": 1,
        "T": 2,
        "F": 3
      }
    },
    1: {
        a: {
          "+": [false, 4],
          "": true
        }
    },
    2: {
      a: {
        "+": [true, 2],
        "*": [false, 7],
        "": [true, 2]
      }
    },
    3: {
      a: {
        "+": [true, 4],
        "*": [true, 4],
        "": [true, 4]
      }
    },
    4: {
      a: {
        "i": [false, 6]
      },
      g: {
        "T": 5,
        "F": 3
      }
    },
    5: {
      a: {
        "+": [true, 1],
        "*": [false, 7],
        "": [true, 1]
      }
    },
    6: {
      a: {
        "+": [true, 5],
        "*": [true, 5],
        "": [true, 5]
      }
    },
    7: {
      a: {
        "i": [false, 6]
      },
      g: {
        "F": 8
      }
    },
    8: {
      a: {
        "+": [true, 3],
        "*": [true, 3],
        "": [true, 3]
      }
    }
  })
  , item0: Item<typeof lang1> = itemFromSituations(Object.values({
    0: [0, 0, end],
    1: [1, 0, end],
    2: [2, 0, end],

    // from 1
    3: [1, 0, "+"],
    4: [2, 0, "+"],

    //from 2
    5: [3, 0, ""],
    6: [4, 0, ""],

    //from 3
    7: [3, 0, "+"],
    8: [4, 0, "+"],

    9: [3, 0, "*"],
    10: [4, 0, "*"],

    11: [5, 0, ""],
    12: [5, 0, "+"],
    //from 7?
    13: [5, 0, "*"],
  }))
  , item1: Item<typeof lang1> = itemFromSituations([
    [0, 1, end],
    [1, 1, end],
    [1, 1, "+"],
  ])
  , item2: Item<typeof lang1> = itemFromSituations([
    [2, 1, end],
    [3, 1, end],
    [2, 1, "+"],
    [3, 1, "+"],
    [3, 1, "*"]
  ])
  , item3: Item<typeof lang1> = itemFromSituations([
    [4, 1, end],
    [4, 1, "+"],
    [4, 1, "*"]
  ])
  , item4: Item<typeof lang1> = itemFromSituations([
    [1, 2, end],
    [1, 2, "+"],
    [3, 0, end],
    [4, 0, end],
    [3, 0, "+"],
    [4, 0, "+"],
    [5, 0, end],
    [5, 0, "+"],
    [3, 0, "*"],
    [4, 0, "*"],
    [5, 0, "*"],
  ])
  , item5: Item<typeof lang1> = itemFromSituations([
    [1, 3, end],
    [1, 3, "+"],
    [3, 1, end],
    [3, 1, '+'],
    [3, 1, "*"],
  ])
  , item6: Item<typeof lang1> = itemFromSituations([
    [5, 1, end],
    [5, 1, "+"],
    [5, 1, "*"]
  ])
  , item7: Item<typeof lang1> = itemFromSituations([
    [3, 2, end],
    [3, 2, '+'],
    [3, 2, "*"],
    [5, 0, end],
    [5, 0, "+"],
    [5, 0, "*"]
  ])
  , item8: Item<typeof lang1> = itemFromSituations([
    [3, 3, end],
    [3, 3, '+'],
    [3, 3, "*"],
  ])
  describe(parser.name, () => {
    it(':string', () => expect(parser(
      rules1, states1,
      str1
    )).toStrictEqual(
      ast1
    ))

    it(':array', () => expect(parser(
      rules1, states1, arr1
    )).toStrictEqual(
      ast1
    ))

    it('shorter', () => expect(() => parser(
      rules1, states1,
      "i+i*"
    )).toThrowError(errors.actionless))
    it('longer', () => expect(() => parser(
      rules1, states1,
      "i+i*ii"
    )).toThrowError(errors.actionless))
    it('notTerminal', () => expect(() => parser(
      rules1, states1,
      "E"
    )).toThrowError(errors.actionless))
    it('unknown terminal', () => expect(() => parser(
      rules1, states1,
      "x"
    )).toThrowError(errors.actionless))
  })

  describe(closure.name, () => {
    it('item0', () => expect(closure(
      lang1, rules1, itemFromSituations([[0, 0, end]])
    )).toStrictEqual(
      item0
    ))
  })

  describe(gotoFn.name, () => {
    it('i0 --E--> i1', () => expect(gotoFn(
      rules1, item0, new Set(['E'] as const)
    )).toStrictEqual(new Map([
      ["E", item1]
    ])))

    it('i0-', () => expect([...gotoFn(
      rules1, item0
    ).keys()]).toStrictEqual([
      "i", "F", "T", "E"
    ]))
  })

  describe(`${gotoFn.name} |> ${closure.name}`, () => {
    it('i1 <--E-- i0', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item0).get("E")!)
    ).toStrictEqual(
      item1
    ))
    it('i2 <--T-- i0', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item0).get("T")!)
    ).toStrictEqual(
      item2
    ))
    it('i3 <--F-- i0', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item0).get("F")!)
    ).toStrictEqual(
      item3
    ))
    it('i4 <--+-- i1', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item1).get("+")!)
    ).toStrictEqual(
      item4
    ))
    it('i5 <--T-- i4', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item4).get("T")!)
    ).toStrictEqual(
      item5
    ))
    it('i6 <--i-- i4', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item4).get("i")!)
    ).toStrictEqual(
      item6
    ))
    it('i7 <--*-- i2', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item2).get("*")!)
    ).toStrictEqual(
      item7
    ))
    it('i8 <--F-- i7', () => expect(
      closure(lang1, rules1, gotoFn(rules1, item7).get("F")!)
    ).toStrictEqual(
      item8
    ))
  })
})