import { parser, tabling } from "./lr1";
import { rules1, lang1, str1 } from "./lr1.test-data";

it('spec_1', () => expect(
  parser(
    rules1,
    tabling(
      lang1,
      rules1,
    ),
    str1
  )
).toStrictEqual([
  0,
  [
    "E",
    4,
    [
      [
        "E",
        4,
        [
          [
            "T",
            3,
            [
              [
                "F",
                2,
                [
                  [
                    "i",
                    1,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      [
        "+",
        6,
      ],
      [
        "T",
        8,
        [
          [
            "T",
            8,
            [
              [
                "F",
                2,
                [
                  [
                    "i",
                    1,
                  ],
                ],
              ],
            ],
          ],
          [
            "*",
            5,
          ],
          [
            "F",
            7,
            [
              [
                "i",
                1,
              ],
            ],
          ],
        ],
      ],
    ],
  ],
]))