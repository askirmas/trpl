import { delimiters } from "./params.json"
import {
  Situation,
  Item
} from "./definitions"

const {
  situtation: situationDelimiter,
  item: itemDelimiter
} = delimiters

export {
  situationHashing,
  itemHashing
}

function situationHashing<T extends Situation>(source: T) {
  return source.join(situationDelimiter)
}

function itemHashing<T extends Item>(source: T) {
  return [...source.keys()].sort().join(itemDelimiter)
}
