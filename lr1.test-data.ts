import { Language, Rules } from "./definitions"
import { Start } from "./consts"

const terminals = ["+", "*", "i"] as const
, notTerminals = [Start, "E", "T", "F"] as const
, lang1: Language<typeof terminals[number], typeof notTerminals[number]> = {
  terminals,
  notTerminals,
}
, rules1: Rules<typeof lang1> = [
  [Start, "E"],
  ["E", "E", "+", "T"],
  ["E", "T"],
  ["T", "T", "*", "F"],
  ["T", "F"],
  ["F", "i"],
 ]
, str1 = "i+i*i"
, arr1 = ["i", "+", "i", "*", "i"] as const
, ast1 = [
  0,
   [
    "E",
    1,
     [
       [
        "E",
        1,
         [
           [
            "T",
            2,
             [
               [
                "F",
                3,
                 [
                   [
                    "i",
                    6,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
       [
        "+",
        4,
      ],            [
        "T",
        5,              [
           [
            "T",
            5,
             [
               [
                "F",
                3,
                 [
                   [
                    "i",
                    6,
                  ],
                ],
              ],
            ],
          ],
           [
            "*",
            7,
          ],
           [
            "F",
            8,
             [
               [
                "i",
                6,
              ],
            ],
          ],
        ],
      ],
    ],
  ],
]

export {
  lang1, rules1, str1, arr1, ast1
}