import { Language, Rules, LR1Table, Item, Situation, Terminal, Letter, Lex } from "./definitions"
import { errors } from "./params.json"
import { first } from "./common"
import { end } from "./consts"
import { situationHashing, itemHashing } from "./utils"

const {isArray} = Array

export {
  parser,
  tabling,

  closure,
  gotoFn,
  itemFromSituations
}

function tabling<T extends Lex, N extends Lex,L extends Language<T, N>>(lang: L, rules: Rules<L>) {
  const {notTerminals, terminals} = lang
  , vocabulary: Map<string, number> = new Map([['', 0]])
  , items: Item<L>[] = [closure(lang, rules, itemFromSituations([[0, 0, end]]))]
  , acceptance = situationHashing([0, 1, end])
  , graph: Map<number, Map<Letter<L>, number>> = new Map()
  , table: LR1Table<L> = [{a: {}}]

  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    , record = table[i]
    
    for (const hash of item.keys()) {
      const situation = item.get(hash)!
      , {0: ruleIndex, 1: pointer} = situation
      if (pointer === rules[ruleIndex].length - 1)
        record.a[situation[2]] = hash === acceptance ? true : [true, ruleIndex]
    }

    const transitions = gotoFn(rules, item)
    for (const letter of transitions.keys()) {
      const next = closure(lang, rules, transitions.get(letter)!)
      , hash = itemHashing(next)

      let index: number
      if (vocabulary.has(hash))
        index = vocabulary.get(hash)!
      else {
        index = table.push({a: {}}) - 1
        items.push(next)
        vocabulary.set(hash, index)
      }

      if (notTerminals.includes(letter as N)) {
        if (!record.g)
          record.g = {}
        //TODO Conflict check
        record.g![letter as N] = index
      } else /* istanbul ignore else */ if (terminals.includes(letter as T))
        //TODO Conflict check
        record.a[letter as T] = [false, index]
      else
        // check with eval|literal|regex from consts
        throw new Error(`Unknown letter "${letter}"`)

      if (!graph.has(i))
        graph.set(i, new Map().set(letter, index))
      else 
        graph.get(i)!.set(letter, index)      
    }
  }

  return table
}

function parser<L extends Language>(
  rules: Rules<L>,
  table: LR1Table<L>,
  source: string|readonly Terminal<L>[]
) {
  const {length: sourceLength} = source
  , stack: [number, ...Array<[
    Letter<L>,
    number
  ]>] = [0]
  let i = 0

  do {
    const letter = i >= sourceLength ? end : (source[i] as Terminal<L>)
    , {length} = stack
    , curTask = stack[length - 1]
    , action = table[
      isArray(curTask) ? curTask[1] : curTask
    ].a[letter]

    switch (action) {
      case undefined:
        throw new Error(errors.actionless)
      case true:
        return stack
      default:
        /* istanbul ignore next */
        if (!isArray(action))
          throw new Error(errors.actionWrong)

        switch (action[0]) {
          case false: // shift
            stack.push([letter, action[1]])
            i++
            continue
          case true: // reduce
            const ruleIndex = action[1]
            , rule = rules[ruleIndex]
            , notTerm = rule[0]
            // TODO optional, pretty-print
            , children = stack.splice(length - rule.length + 1, length)

            const cur = stack[stack.length - 1]
            , curState = isArray(cur) ? cur[1] : cur
            , {g: g} = table[curState]
            , /* istanbul ignore next */ next = g === undefined ? undefined : g[notTerm]
            /* istanbul ignore next */
            if (next !== undefined)
              //@ts-ignore
              stack.push([notTerm, next!, children])

            continue
        }
    }
  } while (i <= sourceLength)

  /* istanbul ignore next */
  throw new Error(errors.left)
}


function closure<L extends Language>(
  lang: L,
  rules: Rules<L>,
  item: Item<L>
) {
  const {length} = rules
  , firsts = first(lang, rules)
  , situations = [...item.values()]

  for (let i = 0; i < situations.length; i++) {
    const situation = situations[i]
    , rule = rules[situation[0]]
    , start = 1 + situation[1]
    , notTerminal = rule[start]
    /* istanbul ignore next */
    if (!firsts.has(notTerminal))
      continue

    const next = rule[start + 1]
    /* istanbul ignore next */
    , nexts = firsts.get(next)
    ?? [
      next
      ?? situation[2]
    ]
    , {length: nextsLength} = nexts

    for (let i = 0; i < length; i++) {
      const rule = rules[i]
      if (rule[0] !== notTerminal)
        continue
      for (let ni = nextsLength; ni--;) {
        const terminal = nexts[ni]
        , situation = [i, 0, terminal] as const
        , hash = situationHashing(situation)

        if (!item.has(hash)) {
          item.set(hash, situation)
          situations.push(situation)
        }
      }
    }
  }

  return item
}

function gotoFn<L extends Language>(rules: Rules<L>, item: Item<L>, letters?: Set<Letter<L>>) {
  const gotos: Map<Letter<L>, Item<L>> = new Map()
  , situations = [...item.values()]

  for (let i = situations.length; i--;) {
    const situation = situations[i]
    , ruleIndex = situation[0]
    , rule = rules[ruleIndex]
    , pointer = situation[1] + 1
    , letter = rule[pointer]

    if (letter === undefined || letters !== undefined && !letters.has(letter))
      continue

    const next: Situation<L> = [
      ruleIndex,
      pointer,
      situation[2]
    ];

    (gotos.has(letter) ? gotos : gotos.set(letter, new Map()))
    .get(letter)!
    .set(
      situationHashing(next),
      next
    )
  }

  return gotos
}

function itemFromSituations<L extends Language>(situations: Situation<L>[]): Item<L> {
  const $return: Item<L> = new Map(
    situations.map(situation => [situationHashing(situation), situation])
  )
  return $return
}